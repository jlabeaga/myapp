package com.mydomain.myapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class HelloApp {

	@Autowired
	HelloService helloService;
	
	public static void main(String[] args) {

		ApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
		
		HelloApp app = context.getBean(HelloApp.class);

		String name = "Tom";
		System.out.println(app.helloService.hello(name));
	}

}
