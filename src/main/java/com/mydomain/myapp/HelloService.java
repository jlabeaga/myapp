package com.mydomain.myapp;

import org.springframework.stereotype.Service;

@Service
public class HelloService {

	String hello( String name ) {
		if( name == null ) {
			name = "World";
		}
		return "Hello " + name;
	}
}
